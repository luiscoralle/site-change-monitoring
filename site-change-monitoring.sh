#!/bin/bash

MONITORED_URL="https://faiweb.uncoma.edu.ar/"
IS_SITE_ONLINE=$(curl -Is $MONITORED_URL | head -n 1)
if [ -z "$IS_SITE_ONLINE" ]; then
    # Nothing to do...
    echo "The site $MONITORED_URL seems offline..."
    exit 1
fi

FULL_SCRIPTPATH=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$FULL_SCRIPTPATH")
PREVIOUS=$(cat "$SCRIPTPATH/previous.fi")
ACTUAL=$(curl -s $MONITORED_URL | grep -A1 "<h2 itemprop=\"name\">" | head -n 2 | tr -d '[:blank:]' | tr -d "\n" | md5sum | cut -d' ' -f1)

echo "PREVIOUS: $PREVIOUS"
echo "  Actual: $ACTUAL"

if [ "$PREVIOUS" == "$ACTUAL" ]; then
    echo "Are equals"
else
    echo "Are differents"
    echo $ACTUAL > "$SCRIPTPATH/previous.fi"
fi

